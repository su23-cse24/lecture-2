#include <iostream>
using namespace std;

int main() {
    /*
        Create a program that asks the user to input a grade [0, 100]
        and returns the appropriate letter grade.
    */

    int grade;
    cin >> grade;

    if (grade < 0 || grade > 100) {
        cout << "Invalid grade" << endl;
    } else if (grade < 60) {
        cout << "F" << endl;
    } else if (grade < 70) {
        cout << "D" << endl;
    } else if (grade < 80) {
        cout << "C" << endl;
    } else if (grade < 90) {
        cout << "B" << endl;
    } else if (grade <= 100) {
        cout << "A" << endl;
    }

    return 0;
}