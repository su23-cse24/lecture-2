#include <iostream>
#include <cstdlib>
using namespace std;

int main() {

    /*
        Create a number guessing game where the user will be asked to guess a number
        between a certain range. 
        
        Your program should ask the user to input the range, and generate a random number 
        between 1 and the range. 
        
        The user will then attempt to repeatedly guess the random number. If their guess 
        is incorrect, the program should let them know if their guess was too high or too low.
    */

    srand(time(NULL));
    int range;
    cout << "Enter range: ";
    cin >> range;

    int target = (rand() % range) + 1;
    bool guessedCorrectly = false;

    int guess;
    cout << "Enter guess: ";
    while (cin >> guess) {
        if (target == guess) {
            guessedCorrectly = true;
            break;
        }

        if (guess > target) {
            cout << "Too High" << endl;
        } else {
            cout << "Too Low" << endl;
        }

        cout << "Enter guess: ";
    }

    if (guessedCorrectly) {
        cout << "Great job!" << endl;
    } else {
        cout << "Better luck next time" << endl;
        cout << "The number was " << target << endl;
    }

    return 0;
}