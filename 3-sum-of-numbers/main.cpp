#include <iostream>
using namespace std;

int main(int argc, char* argv[]) {

    // ./app
    // argc = 1
    // argv = ["./app"]

    // ./app 1 2 3 4 5
    // argc = 6
    // argv = ["./app", "1", "2", "3", "4", "5"]



    /*
        Create a program that continuously asks the user to input numbers 
        and returns their sum

        Extend program to do the same thing, but with command line arguments
    */

    if (argc == 1) {
        int total = 0;
        int x;

        while (cin >> x) {
            total += x;
        }

        cout << "The total is: " << total << endl;
    } else {
        int total = 0;

        for (int i = 1; i < argc; i++) {
            total += stoi(argv[i]);
        }

        cout << "The total is: " << total << endl;
    }


    return 0;
}